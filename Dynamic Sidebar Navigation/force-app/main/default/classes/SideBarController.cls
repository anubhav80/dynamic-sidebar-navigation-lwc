public with sharing class SideBarController {
    @AuraEnabled(cacheable=true)
    public static List<Sidebar_Navigation_Items__mdt> getNavigationNodes(String applicationType) {
        List<Sidebar_Navigation_Items__mdt> returnRecords =
          new List<Sidebar_Navigation_Items__mdt>();
        List<Sidebar_Navigation_Items__mdt> resultRecords =
        [SELECT MasterLabel, DeveloperName, NMBRC_Order__c, NMBRC_Parent__c, Eligible_For__c
          FROM Sidebar_Navigation_Items__mdt
          WHERE NMBRC_Active__c = true
          AND Base_Path__c = : communityBasePath
          ORDER BY NMBRC_Order__c ASC];

        for (Sidebar_Navigation_Items__mdt each : resultRecords) {
            String eligible = each.Eligible_For__c;
            if(eligible != null && eligible.containsIgnoreCase(applicationType)) {
                returnRecords.add(each);
            }
        }
        return returnRecords;
    }
}
