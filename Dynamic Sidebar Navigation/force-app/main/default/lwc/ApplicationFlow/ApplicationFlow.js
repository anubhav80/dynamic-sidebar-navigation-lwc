import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getNavigationNodes from '@salesforce/apex/SideBarController.getNavigationNodes';


import { registerListener, unregisterAllListeners } from 'c/pubsub';


export default class NhCriminalRecordApplication extends NavigationMixin(LightningElement) {
    @api applicationType;
    

    @track navigationItems;
    @track maxOrder;
    @track minOrder;
    @track showSpinner = false;
    @track brcAppObjectInfo;
    @track brcAppFieldsObj;
    @track stepsArray = [];
    @track stepsObj = {};
    @track pageArray = [];


    @wire(getNavigationNodes, { communityBasePath: 'nhlicensing/s/', applicationType: '$applicationType' })//$basePath
    getNavigationNodes({ error, data }) {
        if (error) {
            console.log('error in parent wire: ', JSON.stringify(error));
        } else if (data) {
            let nodes = {}, results;
            results = JSON.parse(JSON.stringify(data));
            // console.log('results: ', JSON.stringify(results));
            if (results.length) {
                this.maxOrder = parseFloat(results[results.length - 1].NMBRC_Order__c);
                this.minOrder = parseFloat(results[0].NMBRC_Order__c);
            }

            nodes[undefined] = { Name: "Root", items: [] };
            let parentOrder = 0;
            results.forEach(function (item) {
                if (!item.NMBRC_Parent__c) {
                    parentOrder = Math.floor(parentOrder) + 1;
                } else {
                    parentOrder += 0.1;
                }
                nodes[item.DeveloperName] = {
                    key: item.DeveloperName,
                    name: item.MasterLabel,
                    order: (Math.round(parentOrder * 10) / 10).toString(),
                    items: [],
                    isCurrent: false,
                    isComplete: false
                };

            });
            console.log('nodes1: ', JSON.stringify(nodes));
            results.forEach(function (item) {
                // console.log('each: ', JSON.stringify(item));
                nodes[item.NMBRC_Parent__c].items.push(nodes[item.DeveloperName]);
            });
            console.log('nodes2: ', JSON.stringify(nodes));

            let tmpItems = nodes[undefined].items;
            this.currentOrder = this.minOrder;

            while (tmpItems.length) {
                tmpItems[0].isCurrent = true;
                tmpItems = tmpItems[0].items;
            }

            this.navigationItems = nodes[undefined].items;
            // console.log('Items>>', JSON.stringify([...this.navigationItems]));

            this.navigationItems.forEach(element => {
                this.stepsArray.push({ key: element.key, value: element.order });
                this.stepsObj[element.key] = element.order;
                if (parseFloat(element.order, 10) === 1) {
                    this.pageArray.push({ key: element.key, value: true });
                } else this.pageArray.push({ key: element.key, value: false });

                if (element.items && element.items.length > 0) {
                    let parentPageIndex = this.pageArray.findIndex(ele => {
                        return ele.key === element.key;
                    });
                    // console.log('deleting parentPageIndex: ', parentPageIndex);
                    if (parentPageIndex > -1) {
                        this.pageArray.splice(parentPageIndex, 1);
                    }
                    let parentStepIndex = this.stepsArray.findIndex(ele => {
                        return ele.key === element.key;
                    });
                    // console.log('deleting parentStepIndex: ', parentStepIndex);
                    if (parentStepIndex > -1) {
                        this.stepsArray.splice(parentStepIndex, 1);
                    }
                    element.items.forEach(ele => {
                        this.stepsArray.push({ key: ele.key, value: ele.order });
                        this.stepsObj[ele.key] = ele.order;
                        this.pageArray.push({ key: ele.key, value: false });
                    });
                }
            });
            console.log('this.stepsObj: ', JSON.stringify(this.stepsObj));
            console.log('this.stepsArray: ', JSON.stringify(this.stepsArray));
            console.log('this.pageArray: ', JSON.stringify(this.pageArray));

        }
    }

    obsKeysToString(obj, keys, sep) {
        return keys.map(key => obj[key]).filter(v => v).join(', ');
    }


    hideComponents() {
        this.pageArray.forEach(element => {
            element.value = false;
        });
        // console.log('pageArray in hiding: ', JSON.stringify(this.pageArray));
    }


    get showIntroduction() {
        return this.findElementInArray('Introduction');
    }

    get showContactDetails() {
        return this.findElementInArray('Contact_Details');
    }
    get showPersonalInformation() {
        return this.findElementInArray('Personal_Information');
    }

    get showChildCareEligibility() {
        return this.findElementInArray('Child_Care_Eligibility');
    }

    get showCHRIAInformation() {
        return this.findElementInArray('CHRIA_Information');
    }

    get showHouseholdAliasesAndAddresses() {
        return this.findElementInArray('Aliases_and_Addresses');
    }
    get showAliasesForNameChange() {
        return this.findElementInArray('Aliases_Information');
    }

    get showHouseholdEmployment() {
        return this.findElementInArray('Employment');
    }

    get showEmploymentEligibility() {
        return this.findElementInArray('Employment_Eligibility');
    }

    get showDocuments() {
        return this.findElementInArray('Documents');
    }

    get showPayments() {
        return this.findElementInArray('Payment');
    }

    get showReviewSubmit() {
        return this.findElementInArray('Review_and_Submit');
    }

    get getCurrentPage() {
        let pages = this.pageArray.filter(element => {
            return element.value === true;
        });
        console.log('showExitButtons: ', JSON.stringify(pages));
        return pages.length > 0 ? pages[0].key : null;
    }

    
    handleSidebarClick(event) {
        console.log('in grandparent: ', JSON.stringify(event.detail));
        console.log('stepsArray: ', JSON.stringify(this.stepsArray));
        console.log('stepsObj: ', JSON.stringify(this.stepsObj));
        console.log('pageArray: ', JSON.stringify(this.pageArray));
        console.log('navigationItems: ', JSON.stringify(this.navigationItems));

        try {
            let data = event.detail;

            let currentIndex = this.stepsArray.findIndex(ele => {
                return parseFloat(ele.value, 10) === parseFloat(data.order);
            });
            if (data.items.length === 0 && currentIndex != -1) {
                this.hideComponents();
                this.removeIsCurrent(data.order);
                this.pageArray[currentIndex].value = true;
            }
            if (data.items.length > 0) {
                this.removeIsCurrent(data.order);
            }

        } catch (error) {
            console.log('error in sidebar: ', error);
        }
    }

    removeIsCurrent(order) {
        this.navigationItems.forEach(element => {
            element.isCurrent = false;
            if (element.items.length > 0) {
                element.items.forEach(child => {
                    child.isCurrent = false;
                });
            }
        });
        console.log('order: ', parseFloat(order));
        let currentIndex = this.navigationItems.findIndex(ele => {
            return parseFloat(ele.order, 10) === Math.floor(parseFloat(order));
        });
        console.log('currentIndex: ', currentIndex);
        if (currentIndex != -1) {
            this.navigationItems[currentIndex].isCurrent = true;
            this.navigationItems[currentIndex].items.forEach(element => {
                if (parseFloat(element.order, 10) === parseFloat(order)) {
                    element.isCurrent = true;
                }
            });
        }
    }

    findElementInArray(param) {
        let val = this.pageArray.findIndex(ele => {
            return ele.key === param;
        });
        // console.log('val: ', val);
        if (val != -1) {
            val = this.pageArray[val];
        }
        return val.value;
        // this.pageArray.find(ele => {
        //     console.log('ele.key: ' + ele.key + ' ; param: ' + param);
        //     ele.key === param;
        // });
    }

    handleIncomingData(event) {
        this.allPageData = { ...event.detail.thisPageData };
    }


    handleNext(which) {
        try {
            // let which = event.target.dataset.page;
            // console.log('step: ', event.target.dataset.page);
            // console.log('step: ', event.currentTarget.dataset.page);
            this.hideComponents();
            // this.showSpinner = true;
            // this.handleNextCalculation(which);
            let currentIndex = this.stepsArray.findIndex(ele => {
                // console.log('parseFloat(ele.value, 10):: ', parseFloat(ele.value, 10));
                // console.log('parseFloat(which):: ', parseFloat(which));
                return parseFloat(ele.value, 10) === parseFloat(which);
            });
            // console.log('currentIndex: ', currentIndex);
            // console.log('pageArray length: ', this.pageArray.length);

            // this.pageArray[currentIndex].value = false;
            if (this.pageArray.length > currentIndex) {
                this.pageArray[currentIndex + 1].value = true;
            }
            // console.log('pageArray: ', JSON.stringify(this.pageArray));

            let navItemCurrentIndex;
            this.navigationItems.forEach(element => {
                // console.log('element.order: ', element.order);
                // console.log('Math.floor(parseFloat(which)): ', Math.floor(parseFloat(which)));
                if (parseFloat(element.order) === Math.floor(parseFloat(which))) {
                    navItemCurrentIndex = this.navigationItems.indexOf(element);
                    if (element.items && element.items.length > 0) {
                        let childItemNavIndex;
                        element.items.forEach(el => {
                            if (parseFloat(el.order) === parseFloat(which)) {
                                el.isComplete = true;
                                el.isCurrent = false;
                                childItemNavIndex = element.items.indexOf(el);
                            }
                        });
                        if (element.items.length == childItemNavIndex + 1) {
                            element.items[childItemNavIndex].isCurrent = false;
                            element.isComplete = true;
                            element.isCurrent = false;
                            if (this.navigationItems.length > navItemCurrentIndex) {
                                this.navigationItems[navItemCurrentIndex + 1].isCurrent = true;
                                if (this.navigationItems[navItemCurrentIndex + 1].items && this.navigationItems[navItemCurrentIndex + 1].items.length > 0) {
                                    this.navigationItems[navItemCurrentIndex + 1].items[0].isCurrent = true;
                                }
                            }
                        }
                        else if (element.items.length > childItemNavIndex) {
                            element.items[childItemNavIndex + 1].isCurrent = true;
                        }
                    } else {
                        element.isComplete = true;
                        element.isCurrent = false;
                        if (this.navigationItems.length > navItemCurrentIndex) {
                            this.navigationItems[navItemCurrentIndex + 1].isCurrent = true;
                            if (this.navigationItems[navItemCurrentIndex + 1].items && this.navigationItems[navItemCurrentIndex + 1].items.length > 0) {
                                this.navigationItems[navItemCurrentIndex + 1].items[0].isCurrent = true;
                            }
                        }
                    }
                }
            });
            // console.log('this.navigationItems: ', JSON.stringify(this.navigationItems));
        } catch (error) {
            console.log('error in next: ', error);

        }
    }

    handleBack(event) {
        try {
            let which = event.target.dataset.page;
            this.hideComponents();
            let currentIndex = this.stepsArray.findIndex(ele => {
                // console.log('parseFloat(ele.value, 10):: ', parseFloat(ele.value, 10));
                // console.log('parseFloat(event.target.dataset.page):: ', parseFloat(event.target.dataset.page));
                return parseFloat(ele.value, 10) === parseFloat(which);
            });
            // console.log('currentIndex: ', currentIndex);
            // console.log('pageArray length: ', this.pageArray.length);

            // this.pageArray[currentIndex].value = false;
            if (currentIndex != 0) {
                this.pageArray[currentIndex - 1].value = true;
            }

            let navItemCurrentIndex;
            this.navigationItems.forEach(element => {
                // console.log('element.order: ', element.order);
                // console.log('Math.floor(parseFloat(which)): ', Math.floor(parseFloat(which)));
                if (parseFloat(element.order) === Math.floor(parseFloat(which))) {
                    navItemCurrentIndex = this.navigationItems.indexOf(element);
                    if (element.items && element.items.length > 0) {
                        let childItemNavIndex;
                        element.items.forEach(el => {
                            if (parseFloat(el.order) === parseFloat(which)) {
                                el.isCurrent = false;
                                childItemNavIndex = element.items.indexOf(el);
                            }
                        });
                        if (childItemNavIndex === 0) {
                            element.items[childItemNavIndex].isCurrent = false;
                            element.isCurrent = false;
                            if (this.navigationItems.length > navItemCurrentIndex) {
                                this.navigationItems[navItemCurrentIndex - 1].isCurrent = true;
                                if (this.navigationItems[navItemCurrentIndex - 1].items && this.navigationItems[navItemCurrentIndex - 1].items.length > 0) {
                                    this.navigationItems[navItemCurrentIndex - 1].items[this.navigationItems[navItemCurrentIndex - 1].items.length - 1].isCurrent = true;
                                }
                            }
                        }
                        else if (element.items.length > childItemNavIndex) {
                            element.items[childItemNavIndex - 1].isCurrent = true;
                        }
                    } else {
                        element.isCurrent = false;
                        if (this.navigationItems.length > navItemCurrentIndex) {
                            this.navigationItems[navItemCurrentIndex - 1].isCurrent = true;
                            if (this.navigationItems[navItemCurrentIndex - 1].items && this.navigationItems[navItemCurrentIndex - 1].items.length > 0) {
                                this.navigationItems[navItemCurrentIndex - 1].items[this.navigationItems[navItemCurrentIndex - 1].items.length - 1].isCurrent = true;
                            }
                        }
                    }
                }
            });

            // console.log('this.navigationItems: ', JSON.stringify(this.navigationItems));
        } catch (error) {
            console.log('error in back: ', error);
        }
    }

    findKeyByValue() {
        return this.pageArray.filter(element => {
            element.value === true;
        })
    }

}